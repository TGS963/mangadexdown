# mangadexdown

Downloader for mangas from mangadex written in C++

This probably only works on linux systems at the moment

# Dependecies

Need to install nlohmann-json and libcurl package to compile locally(Not needed to just run the binary)

# Installation

Run in the directory

`make`

Although a binary is uploaded, building it yourself is recommended

# Run

`./main`
and then put the mangadex code in the prompt

If want to use without the `./` just copy the binary to /usr/bin


You can rename the binary as you want :)

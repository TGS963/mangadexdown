#include <bits/stdc++.h>
#include <nlohmann/json.hpp>
#include <curl/curl.h>
//#include <libxml2/libxml/HTMLparser.h>
#include <fstream>
#include <algorithm>

using json = nlohmann::json;
using namespace std;

void getchapters(const char* url, vector<string> &chapter, string &name, vector<string> &chapname){
  CURL *curl;
  CURLcode res;
  FILE *html;

  curl_global_init(CURL_GLOBAL_ALL);
  string a;

  curl = curl_easy_init();

  if (curl){
    html = fopen("manga.json", "w");
    curl_easy_setopt(curl, CURLOPT_URL, url);
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, NULL);
    curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, html);

    res = curl_easy_perform(curl);
    a = curl_easy_strerror(res);

    if (res != CURLE_OK){
      cout << "error is " << curl_easy_strerror(res) << endl;
    }

    curl_easy_cleanup(curl);
  }
  fclose(html);
  curl_global_cleanup();

  ifstream ifs("manga.json");
  json j;
  ifs >> j;
  ifs.close();

  name = j["manga"]["title"];

  json j2 = j["chapter"];
  int numbering = 1;
  cout << "\n\nChapters of the manga in English" << endl;
  for (const auto& i : j2.items()){
    if(i.value()["lang_name"] == "English"){
      cout << numbering++ << ". Volume:" << i.value()["volume"] << " Chapter:" << i.value()["chapter"] << " Title:" << i.value()["title"] << endl;
      chapter.push_back(i.key());
      string vol = i.value()["volume"];
      string chapter = i.value()["chapter"];
      string title = i.value()["title"];
      string tmp = "vol. " + vol + " chap. " + chapter + " - " + title;
      chapname.push_back(tmp);
    }
  }

  system("rm -f manga.json");
}

void downloadpages(const char* url, const char* name){
  CURL *curl;
  CURLcode res;
  FILE *img;

  curl_global_init(CURL_GLOBAL_ALL);

  curl = curl_easy_init();

  if (curl){
    img = fopen(name, "w");
    curl_easy_setopt(curl, CURLOPT_URL, url);
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, NULL);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, img);

    res = curl_easy_perform(curl);

    if (res != CURLE_OK){
      cout << "error is " << curl_easy_strerror(res) << endl;
    }

    curl_easy_cleanup(curl);
  }
  fclose(img);

  curl_global_cleanup();
  if (res != CURLE_OK){
    downloadpages(url, name);
  }
}

void getpages(const char* url, string dir){
  CURL *curl;
  CURLcode res;
  FILE *html;

  curl_global_init(CURL_GLOBAL_ALL);
  string a;

  curl = curl_easy_init();

  if (curl){
    html = fopen("manga.json", "w");

    curl_easy_setopt(curl, CURLOPT_URL, url);
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, NULL);
    curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, html);

    res = curl_easy_perform(curl);
    a = curl_easy_strerror(res);

    if (res != CURLE_OK){
      cout << "error is " << curl_easy_strerror(res) << endl;
    }

    curl_easy_cleanup(curl);
  }
  fclose(html);
  curl_global_cleanup();

  ifstream ifs("manga.json");
  json j;
  ifs >> j;
  ifs.close();
  // write prettified JSON to another file
  /*ofstream o("pretty.json");
  o << setw(4) << j << std::endl;
  o.close();*/
  json j2 = j["page_array"];
  vector<string> pages, image;
  for (const auto& ole : j2.items()){
    string server, hash, img;
    server = j["server"];
    hash = j["hash"];
    img = ole.value();
    pages.push_back(server + hash + "/" + img);
    image.push_back(img);
  }

  auto x = image.begin();
  for (auto i : pages){
    string tmp = dir + "/" + *x;
    tmp.erase(remove(tmp.begin(), tmp.end(), '\"'), tmp.end());
    downloadpages(i.c_str(), tmp.c_str());
    x++;
  }

  system("rm -f manga.json");
}

int main(){
  string url, dir, name;
  vector<string> chapters, subdir;
  cout << "Enter the mangadex code for the manga: ";
  cin >> url;
  url = "https://mangadex.org/api/?id=" + url + "&type=manga";
  getchapters(url.c_str(), chapters, name, subdir);

  int start, end;
  cout << endl << "Enter the starting chapter index and ending chapter index you want to download(space separated): ";
  cin >> start >> end;

  chapters.erase(chapters.begin() + end, chapters.end());
  subdir.erase(subdir.begin() + end, subdir.end());
  if (start != 1){
    chapters.erase(chapters.begin(), chapters.begin() + start - 1);
    subdir.erase(subdir.begin(), subdir.begin() + start - 1);
  }

  cout << "Enter absolute path of where you wanna save: ";
  cin >> dir;
  if (dir[dir.size()-1] != '/'){
    dir.append("/");
  }
  //cout << endl << name << endl << imglink << endl << last << endl;
  for (auto i : subdir){
    string mkdir = "mkdir -p \"" + dir + name + "/" + i + "\"";

    system(mkdir.c_str());
  }

  auto j = chapters.begin();
  for (auto i : subdir){
    string link = "https://mangadex.org/api/?id=" + *j + "&server=null&saver=1&type=chapter";

    getpages(link.c_str(), dir + name + "/" + i);
    j++;
  }
  cout << "Done downloading!!" << endl;
  return 0;
}
